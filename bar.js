(function functionName(d3, window, BarChart) {
	if (d3) {
		window.BarChart = BarChart;
	}
}(d3, this,
	function (targetId) {
		var width = 600,
			height = 400,
			margin = { top: 20, right: 40, bottom: 20, left: 50 },
			responsive = true;
		var axisBottomTickFormat;
		var axisLeftTickFormat;
		var valueName = "Value";
		var dateName = "Date";
		var emptyMessage = "Data not found for visualization";
		var saveData = [];

		/*==========================================================================
		utilities functions
		============================================================================*/

		function emptyBarChart(currentTarget, text) {
			d3.select(currentTarget)
				.html("")
				.append("div")
				.style("width", "100%")
				.style("position", "absolute")
				.style("top", (height / 2) + "px")
				.style("text-align", "center")
				.text(text);
		}

		function getWidthOfTicks(svg) {
			var gxAxis = svg.select(".axis-bottom");
			var tickWidth = 0;
			var marginTicks = 0;
			var arrTicksWidth = [];
			var allTicks = gxAxis.selectAll(".tick").each(function (d, i) {
				var w = d3.select(this).select("text").node().getBBox().width;
				tickWidth += w;
				arrTicksWidth.push(w);
			});
			var totalWidth = tickWidth + (margin.left + margin.right);
			var maxTickWidth = d3.max(arrTicksWidth);
			return { 'totalWidth': totalWidth, 'maxTickWidth': maxTickWidth };
		}
		//get min or max - "calculate" function ([d3.min || d3.max])
		function getValue(data, calculate) {
			var values = [];
			data.forEach(function (d, i) {
				values.push(calculate(d.data.map(function (k, i) { return k[valueName]; })));
			});
			var calculateValue = calculate(values);
			var moreCalculate = (10 * calculateValue) / 100;
			return calculateValue + moreCalculate;
		}
		//get Dates from  data
		function getDates(data) {
			if (data.length) {
				if (data[0].data.length) {
					return data[0].data.map(function (d, i) {
						return d[dateName];
					});
				} else {
					return data[1].data.map(function (d, i) {
						return d[dateName];
					});
				}
			}
		}
		/*==========================================================================
				Main Function
		============================================================================*/
		function barchart(enterData) {
			if (enterData) {
				saveData = enterData;
			}
			var currentData = saveData;
			d3.selectAll(targetId).each(function (d, i) {
				d3.select(this).html("");
				//if responsive I get the width
				if (responsive) {
					targetWidth = parseFloat(d3.select(this).style("width").replace("px", ""));
					targetHeight = parseFloat(d3.select(this).style("height").replace("px", ""));
					if (targetWidth > 0) width = targetWidth;
					if (targetHeight > 0) height = targetHeight;
				}

				//si el array esta vacio limpio el grafico
				if (!currentData.length) {
					emptyBarChart(this, emptyMessage);
					return;
				}
				var min = getValue(currentData, d3.min);
				var max = getValue(currentData, d3.max);


				if ((min === undefined || max === undefined) || (isNaN(min) || isNaN(max))) {
					emptyBarChart(this, emptyMessage);
					return;
				}

				if (Math.abs(min) > max) {
					max = Math.abs(min);
				}

				var dates = getDates(currentData).reverse();


				//crate svg
				var svg = d3.select(this)
					.append("svg")
					.attr("width", width)
					.attr("height", height);

				//crate "g" containers to axis
				var axisBottomSelection = svg.append("g").attr("class", "axis-bottom").attr("transform", "translate(" + [margin.left, (height - margin.bottom)] + ")");
				var axisLeftSelection = svg.append("g").attr("class", "axis-left").attr("transform", "translate(" + [margin.left, margin.top] + ")");
				var divisorLineSelection = svg.append("g").attr("transform", "translate(" + [margin.left, 0] + ")")
				//sacale definition
				var x = d3.scaleBand()
					.domain(dates)
					.rangeRound([0, width - (margin.left + margin.right)])
					.paddingInner(0.2);

				var x1 = d3.scaleBand()
					.domain(d3.keys(currentData))
					.rangeRound([0, x.bandwidth()])
					.padding(0.2);

				var y = d3.scaleLinear()
					.domain([-(max), max]).nice()
					.range([height - margin.top - margin.bottom, 0]);

				//draw line by end months
				divisorLineSelection
					.attr("class", "month-divisor-line")
					.selectAll("line")
					.data(dates)
					.enter()
					.append("line")
					.attr("stroke", "red")
					.attr("stroke-width", "1.5px")
					.attr("x1", function (d, i) {
						if (i !== 0) {
							return x(d) - (x.step() / 2) / dates.length;
						} else return 0;
					})
					.attr("y1", function (d, i) {
						if (i !== 0) {
							return margin.top;
						} else return 0;
					})
					.attr("x2", function (d, i) {
						if (i !== 0) {
							return x(d) - (x.step() / 2) / dates.length;
						} else return 0;
					})
					.attr("y2", function (d, i) {
						if (i !== 0) {
							return height - margin.bottom;
						} else return 0;
					});

				//axis bottom
				var axisBottom = d3.axisBottom(x)
					.tickFormat(typeof axisBottomTickFormat === "function" ? axisBottomTickFormat : function (d, i) {
						return d3.timeFormat("%b, %Y")(new Date(d));
					})
					// .tickSize(-(height - (margin.top + margin.bottom)));
					.tickSize(8);

				//axis left
				var axisLeft = d3.axisLeft(y)
					.tickSize(-(width - (margin.left + margin.right)))
					.tickSize(8)
					.tickPadding(5)
					.tickFormat(function (d, i) {
						if (d < 0) {
							d3.select(this).attr("class", "negative-text");
						} else if (d > 0) {
							d3.select(this).attr("class", "positive-text");
						}
						return typeof axisLeftTickFormat === "function" ? axisLeftTickFormat.call(this, d) : d;
					}).ticks(6);

				//draw axis
				axisBottomSelection.call(axisBottom);
				axisLeftSelection.call(axisLeft);


				//calculate width of axis bottom
				var size_width = getWidthOfTicks(svg);//It may be better
				//re draw axis bottom
				if (size_width.totalWidth >= (width - margin.left - margin.right)) {
					var maxText = size_width.maxTickWidth;
					axisBottom
						// .tickSize(-(height - (margin.top + margin.bottom + maxText)));
						.tickSize(8);

					axisBottomSelection
						.attr("transform", "translate(" + [margin.left, (height - (margin.bottom + maxText))] + ")")
						.call(axisBottom)
						.selectAll("text")
						.attr("transform", "rotate(60)")
						.style("text-anchor", "start")
						.attr("y", "0");

					//updated 'y' scale
					y.range([height - (margin.top + margin.bottom + maxText), 0]);
					var axisLeft2 = d3.axisLeft(y)
						.tickSize(8)
						.tickPadding(5)
						.tickFormat(function (d, i) {
							if (d < 0) {
								d3.select(this).attr("class", "negative-text");
							} else if (d > 0) {
								d3.select(this).attr("class", "positive-text");
							}
							return typeof axisLeftTickFormat === "function" ? axisLeftTickFormat.call(undefined, d) : d;
						}).ticks(6);
					axisLeftSelection.call(axisLeft2);

					x.rangeRound([0, width - (margin.left + margin.right)]);
					x1.rangeRound([0, x.bandwidth()]);

					divisorLineSelection.selectAll("line")
						.attr("y1", function (d, i) {
							if (i !== 0) {
								return height - (margin.bottom + maxText);
							} else return 0;
						})
						.attr("y2", function (d, i) {
							if (i !== 0) {
								return height - (margin.bottom + maxText);
							} else return 0;
						});
				}

				svg.append("g")
					.attr("transform", function (d, i) { return "translate(" + [margin.left, margin.top] + ")"; })
					.append("rect")
					.attr("class", "negative-values")
					.attr("x", function (d, i) {
						return 0;
					})
					.attr("y", function (d) {
						return y(0);
					})
					.attr("width", d3.max(x.range()))
					.attr("height", y(0));

				svg.append("g")
					.attr("transform", function (d, i) { return "translate(" + [margin.left, margin.top] + ")"; })
					.append("rect")
					.attr("class", "positive-values")
					.attr("x", function (d, i) {
						return 0;
					})
					.attr("y", function (d) {
						return 0;
					})
					.attr("width", d3.max(x.range()))
					.attr("height", y(0));


				var barSelections = svg.append("g")
					.selectAll("g")
					.data(currentData)
					.enter()
					.append("g")
					.attr("class", "rect-container")
					.attr("transform", function (d, i) { return "translate(" + [margin.left, margin.top] + ")"; })
					.selectAll('rect')
					.data(function (d, i) {
						return d.data.map(function (m, k) {
							m.fill = d.options.fill || d3.schemeCategory20[i];
							m.key = i;
							return m;
						});
					})
					.enter().append('rect')
					.attr("transform", function (d, i) { return "translate(" + [x(d[dateName]), 0] + ")"; })
					.attr("class", "bar")
					.attr("fill", function (d, i) {
						return d.fill;
					})
					.attr("width", x1.bandwidth())
					.attr("height", function (d) {
						return Math.abs(y(d[valueName]) - y(0));
					})
					.attr("x", function (d, i) {
						return x1(d.key);
					})
					.attr("y", function (d) {
						return y(Math.max(0, d[valueName]));
					});


				barSelections
					.append("text")
					.text(function (d, i) {
						this.parentNode.parentElement.appendChild(this);
						return typeof rectTextFormat === "function" ? rectTextFormat.call(this, d[valueName]) : d[valueName];
					})
					.attr("transform", function (d, i) { return "translate(" + [x(d[dateName]), 0] + ")"; })
					.attr("class", function (d, i) {
						if (d[valueName] < 0) {
							return "negative-text";
						} else if (d[valueName] > 0) {
							return "positive-text";
						}
						return "";
					})
					.attr("x", function (d, i) {
						return x1(d.key) + 5;
					})
					.attr("y", function (d) {
						return y(Math.max(0, d[valueName])) + (d[valueName] < 0 ? Math.abs(y(d[valueName]) - y(0)) + 12 : -5);
					});

			});
			return barchart;
		}

		barchart.valueName = function (value) {
			if (!arguments.length) return valueName;
			valueName = value;
			return barchart;
		};
		barchart.dateName = function (value) {
			if (!arguments.length) return dateName;
			dateName = value;
			return barchart;
		};
		barchart.width = function (value) {
			if (!arguments.length) return barchart;
			width = value;
			return barchart;
		};

		barchart.axisBottomTickFormat = function (value) {
			if (!arguments.length) return axisBottomTickFormat;
			axisBottomTickFormat = value;
			return barchart;
		};
		barchart.axisLeftTickFormat = function (value) {
			if (!arguments.length) return axisLeftTickFormat;
			axisLeftTickFormat = value;
			return barchart;
		};
		barchart.rectTextFormat = function (value) {
			if (!arguments.length) return rectTextFormat;
			rectTextFormat = value;
			return barchart;
		};
		barchart.saveData = function (value) {
			if (!arguments.length) return saveData;
			saveData = value;
			return barchart;
		};
		barchart.emptyMessage = function (value) {
			if (!arguments.length) return emptyMessage;
			emptyMessage = value;
			return barchart;
		};
		var id;
		d3.select(window).on('resize.updatesvg', function (d, i) {
			clearTimeout(id);
			if (saveData)
				id = setTimeout(barchart, 1000);
		});

		return barchart;
	}));

var bar1 = [
	{ "Date": new Date(2017, 0, 0), "Value": 40 },
	{ "Date": new Date(2017, 1, 0), "Value": 60 },
	{ "Date": new Date(2017, 2, 0), "Value": -40 },
	{ "Date": new Date(2017, 3, 0), "Value": 10 },
	{ "Date": new Date(2017, 4, 0), "Value": -60.5 }
];
var bar2 = [
	{ "Date": new Date(2017, 0, 0), "Value": 33 },
	{ "Date": new Date(2017, 1, 0), "Value": -38 },
	{ "Date": new Date(2017, 2, 0), "Value": 77.5 },
	{ "Date": new Date(2017, 3, 0), "Value": -11 },
	{ "Date": new Date(2017, 4, 0), "Value": 55 }
];
var bar3 = [
	{ "Date": new Date(2017, 0, 0), "Value": 63 },
	{ "Date": new Date(2017, 1, 0), "Value": -8 },
	{ "Date": new Date(2017, 2, 0), "Value": 5 },
	{ "Date": new Date(2017, 3, 0), "Value": -11 },
	{ "Date": new Date(2017, 4, 0), "Value": 55 }
];
var data = [
	{ data: bar1, options: {} },
	{ data: bar2, options: {} },
	{ data: bar3, options: {} }
];
var barchart = new BarChart("#bar");

barchart
	.axisLeftTickFormat(function (d, i) {
		return d3.format(".1%")(d / 100);
	})
	.rectTextFormat(function (d, i) {
		return d3.format(".2%")(Math.abs(d / 100));
	})(data);


